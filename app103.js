//outer function
function greet(){

    const name = 'Mr.John Doe'    //<--|
    //inner function                 //|
    function displayName(){          //| Closer Function
        return "Hi: "+" " + name     //| 
    }                                //|
    return displayName            //<--|

}
const g1 = greet()
console.log(g1)
console.log(g1())